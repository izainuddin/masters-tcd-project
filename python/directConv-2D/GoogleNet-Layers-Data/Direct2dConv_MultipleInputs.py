'''
Direct Convolution (2D Correlation) Algorithm
Developer: Zain Uddin
'''
import numpy as np
from scipy import signal
import datetime;
import importlib
from glob import glob
input_module_files = glob("input_data_python\\*.py")
kernel_module_files = glob("kernel_data_python\\*.py")
fr = open("results_python\\analysis_py_result.txt", "w")
for i in range(len(input_module_files)):
    heading= "GoogleNet " + str(input_module_files[i][25:].replace('.py',''))
    layerName = input_module_files[i][25:].replace('.py','')
    input_module_name=input_module_files[i].replace('.py','')
    input_module_name=input_module_name.replace('\\','.')
    input_module = importlib.import_module(input_module_name)
    X_in = input_module.X_in
    # ----------------------------
    kernel_module_name=kernel_module_files[i].replace('.py','')
    kernel_module_name=kernel_module_name.replace('\\','.')
    kernel_module = importlib.import_module(kernel_module_name)
    H = kernel_module.H

    M = len(H[0][0])
    C = len(X_in[0][0])	  
    in_rows = 4
    in_cols = 4
    k_rows = 3
    k_cols = 3
    o = 2
    X_size = len(X_in[0])

    # Pad the input with zeros around.
    x = (C, X_size+2,X_size+2)
    X_padded = np.zeros(x)

    for ch in range(C):
        for r in range(X_size):
            for c in range(X_size):
                X_padded[ch][r + 1][c + 1] = X_in[r][c][ch]

    x = (C,int(X_size/o),int(X_size/o),in_rows,in_cols)
    X = np.zeros(x)

    for ch in range(C):
        jmpR = 0
        for in_no_r in range (int(X_size/o)):
            jmpC = 0
            for in_no_c in range (int(X_size/o)):
                for r in range (in_rows):
                    for c in range(in_cols):
                        X[ch][in_no_r][in_no_c][r][c] = X_padded[ch][r+jmpR][c+jmpC]
                jmpC = jmpC + 2
            jmpR = jmpR + 2

    ht = (M,C,k_rows,k_cols)
    Ht = np.zeros(ht)

    for r in range(k_rows):
        for c in range(k_cols):
            for m in range(M):
                for ch in range(C):
                    Ht[m][ch][r][c]=H[r][c][m][ch]

    timeArray = np.zeros(10)
    totalTime = 0;
    for t in range(1):
        # Start timestamp
        start = datetime.datetime.now().timestamp()

        # Direct Convolution
        x = (M, int(X_size/o),int(X_size/o),o,o)
        O = np.zeros(x)
        for m in range(M):
            for op_no_r in range (int(X_size/o)):
                for op_no_c in range (int(X_size/o)):
                    for ch in range(C):
                        O[m][op_no_r][op_no_c] = O[m][op_no_r][op_no_c] + signal.correlate2d(X[ch][op_no_r][op_no_c],Ht[m][ch],'valid')

        # Combine the outputs
        x = (M, X_size,X_size)
        Oc = np.zeros(x)

        for m in range(M):
            rr=0
            for no_r in range(int(X_size/2)):
                for r in range (o):
                    cc=0
                    for no_c in range(int(X_size/2)):
                        for c in range(o):
                            #print (c+cc,c,r+rr,r, no_r)
                            Oc[m][r+rr][c+cc] = O[m][no_r][no_c][r][c]
                            #print (Oc[m][r+rr][c+cc])
                            #input("Enter")
                        cc= cc+2
                rr=rr+2
        # stop timestamp
        stop = datetime.datetime.now().timestamp()
        timeArray[t]= stop-start
    totalTime = np.median(timeArray)
    print (totalTime)
    # write to analysis file
    fr.write(layerName + "\t" + "Processing time is: " + str(totalTime)+'\n')
    # Print the final output
    f = open("results_python\\" + str(layerName)+ "_py_result.txt", "w")
    f.write(heading + '\n')
    f.write("Processing time is: " + str(totalTime)+'\n')
    f.write("-------------------------------------------------"+'\n')
    for m in range(M):
        for r in range(X_size):
            for c in range(X_size):
                f.write(str(int(Oc[m][r][c])))
                f.write('\t')
            f.write('\n')
        f.write('---------------------------------')
        f.write('\n')
    f.close()
fr.close()
