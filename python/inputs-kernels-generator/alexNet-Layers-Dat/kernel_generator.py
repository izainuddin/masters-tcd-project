# Generate Input Data
import numpy as np
import pandas as pd

#identify the name of the architecture
t = pd.read_csv('data-param.csv')
archName = t.columns[0]
t = pd.read_csv('data-param.csv', skiprows = 1)
print(archName)
totalLayers = len(t.values)
for x in range(totalLayers):
    # get name of the layer, Number of Channels, Input H and W
    layerName = t.values[x][0]
    M = t.values[x][3]
    C = t.values[x][4]
    KH = 3
    fp = (2**7)     # Scale to 8 bit value -128 to 127
    h = (KH,KH,M,C)
    H = 2 * np.random.random_sample(h) - 1

    # Print the input data for python

    f = open("C:\masters-tcd-project\python\directConv-2D\GoogleNet-Layers-Data\kernel_data_python\kernels_"+ str(archName) + '_'+ str(layerName)+".py", "w")
    f.write('# Architecture: '+ str(archName)+ "\n")
    f.write('# Layer Name: '+ str(layerName)+ "\n")
    f.write('# ----------------------------------\n')
    f.write('#  H[H][W][M][C] ')
    f.write("# C = " +  str(C)+ "\n")
    f.write("# KH = " +  str(KH)+ "\n")
    f.write('\n\n')
    f.write('H = ')
    f.write('\t\t')
    f.write('[')
    f.write('\n')
    for r in range(KH):
        f.write('[')
        f.write('\n')
        for c in range(KH):
            f.write('[')
            f.write('\n')
            for m in range(M):
                f.write('[')
                for ch in range(C):
                    #f.write(str(int(round(H[m][ch][r][c]*fp))))
                    if ((int(round(H[r][c][m][ch]*fp))) == 128):
                        f.write(str(127))
                    else:
                        f.write(str(int(round(H[r][c][m][ch]*fp))))
                    if (ch!=(C-1)):
                        f.write(',')
                f.write(']')
                if (m!=(M-1)):
                        f.write(',')
                f.write('\n')
            f.write(']')
            if (c!=(KH-1)):
                        f.write(',')
            f.write('\n')
            f.write('#----------------------------------------------')
            f.write('\t\n')
        f.write(']')
        if (r!=(KH-1)):
            f.write(',')
        f.write('#********************************************************')
        f.write('\n\n')
    f.write(']')
    f.write('#===============================================================')
    f.write('\n\n')
    f.close()

    # Print the input data for Cpp

    f = open("C:\masters-tcd-project\c++\input-kernel-data\kernel_data_c++\kernels_"+ str(archName)+ "_" +str(layerName) + ".h", "w")
    f.write('// Architecture: '+ str(archName)+ "\n")
    f.write('// Layer Name: '+ str(layerName)+ "\n")
    f.write('// ----------------------------------\n')
    f.write("#ifndef KERNELS_H"+ "\n")
    f.write("#define KERNELS_H"+ "\n")
    f.write('#include "../../intel/fixedpoints/input_kernel_vars.h" '+ '\n')
    f.write("// K[KH][KW][M][C] "+ "\n")
    f.write("// C = " +  str(C)+ "\n")
    f.write("// KH = " +  str(KH)+ "\n")
    f.write("// KW = " +  str(KH)+ "\n")
    f.write("const int M = " +  str(M)+ ";\n")
    f.write('\n')

    f.write('const float K[3][3][M][C] = ')
    f.write('\t\t')
    f.write('{')
    f.write('\n')
    for r in range(KH):
        f.write('{')
        f.write('\n')
        for c in range(KH):
            f.write('{')
            f.write('\n')
            for m in range(M):
                f.write('{')
                for ch in range(C):
                    #f.write(str(int(round(H[m][ch][r][c]*fp))))
                    if ((int(round(H[r][c][m][ch]*fp))) == 128):
                        f.write(str(127))
                    else:
                        f.write(str(int(round(H[r][c][m][ch]*fp))))
                    if (ch!=(C-1)):
                        f.write(',')
                f.write('}')
                if (m!=(M-1)):
                        f.write(',')
                f.write('\n')
            f.write('}')
            if (c!=(KH-1)):
                    f.write(',')
            f.write('\n')
            f.write('//----------------------------------------------')
            f.write('\t\n')
        f.write('}')
        if (r!=(KH-1)):
            f.write(',')
        f.write('//********************************************************')
        f.write('\n\n')
    f.write('};')
    f.write('//===============================================================')
    f.write('\n\n')

    f.write("#endif")
    f.close()

