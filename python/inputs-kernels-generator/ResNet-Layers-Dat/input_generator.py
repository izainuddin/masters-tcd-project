# Generate Input Data
import numpy as np
import pandas as pd

#identify the name of the architecture
t = pd.read_csv('data-param.csv')
archName = t.columns[0]
t = pd.read_csv('data-param.csv', skiprows = 1)
print(archName)
totalLayers = len(t.values)
for x in range(totalLayers):
    # get name of the layer, Number of Channels, Input H and W
    layerName = t.values[x][0]
    H = t.values[x][1]
    W = t.values[x][2]
    C = t.values[x][4]

    # Calculate the HW_PAD for 5 vectors and 4 Muls Vector Strategy
    # hw must be a factor of 4
    hw_pad_bits = int(4-(((H/2)*(W/2)) %4))
    if (hw_pad_bits<4):
        hw_padded = int( ((H/2)*(W/2))+ hw_pad_bits )
    else:
        hw_padded = int( (H/2)*(W/2) )
        
    

    fp = (2**7)     # Scale to 8 bit value -128 to 127
    x = (H,W,C)
    X = 2 * np.random.random_sample(x) - 1

    # Print the input data for python

    f = open("C:\masters-tcd-project\python\directConv-2D\GoogleNet-Layers-Data\input_data_python\inputs_"+ str(archName) + '_'+ str(layerName)+".py", "w")
    f.write('# Architecture: '+ str(archName)+ "\n")
    f.write('# Layer Name: '+ str(layerName)+ "\n")
    f.write('# ----------------------------------\n')
    f.write('#  X[C][H][W] ')
    f.write("# H = " +  str(H)+ "\n")
    f.write("# W = " +  str(W)+ "\n")
    f.write("# C = " +  str(C)+ "\n")
    f.write('\n\n')
    f.write('X_in = ')
    f.write('\t\t')
    f.write('[')
    f.write('\n')
    for r in range(H):
        f.write('[')
        f.write('\n')
        for c in range(W):
            f.write('[')
            for ch in range(C):
                #f.write(str(int(round(X[ch][r][c]*fp))))
                if ((int(round(X[r][c][ch]*fp))) == 128):
                    f.write(str(127))
                else:
                    f.write(str(int(round(X[r][c][ch]*fp))))
                if (ch!=(C-1)):
                    f.write(',')
            f.write(']')
            if (c!=(W-1)):
                    f.write(',')
            f.write('\n')
        f.write(']')
        if (r!=(H-1)):
                    f.write(',')
        f.write('\n')
        f.write('#----------------------------------------------')
        f.write('\t\n')
    f.write(']')
    f.write('#********************************************************')
    f.write('\n\n')
    f.close()

    # Print the input data for Cpp

    f = open("C:\masters-tcd-project\c++\input-kernel-data\input_data_c++\inputs_"+ str(archName)+ "_" +str(layerName) + ".h", "w")
    f.write('// Architecture: '+ str(archName)+ "\n")
    f.write('// Layer Name: '+ str(layerName)+ "\n")
    f.write('// ----------------------------------\n')
    f.write("#ifndef INPUTS_H"+ "\n")
    f.write("#define INPUTS_H"+ "\n")
    f.write("// X[H][W][C] "+ "\n")
    f.write("// H = " +  str(H)+ "\n")
    f.write("// W = " +  str(W)+ "\n")
    f.write("// C = " +  str(C)+ "\n")
    f.write("const int hw_pad =" +str(hw_padded) + ";\n")
    f.write('const char *fr = "results_c++/' +str(archName) + '_' +str(layerName)+'_c++_result.txt" ;\n')
    f.write('const char *arch = "' +str(archName) + '";\n')
    f.write('const char *layerName = "' +str(layerName) + '";\n')
    f.write("const int C =" +str(C) + ";\n")
    f.write("const int input_len =" +str(H) + ";\n")

    # Convert floats to 8 bit integers
    f.write('const float X_in[input_len][input_len][C] = ')
    f.write('\t\t')
    f.write('{')
    f.write('\n')
    for r in range(H):
        f.write('{')
        #f.write('\n')
        for c in range(W):
            f.write('{')
            for ch in range(C):
                #f.write(str (int(round(X[r][c][ch]*fp))) )
                if ((int(round(X[r][c][ch]*fp))) == 128):
                    f.write(str(127))
                else:
                    f.write(str(int(round(X[r][c][ch]*fp))))
                if (ch!=(C-1)):
                    f.write(',')
            f.write('}')
            if (c!=(W-1)):
                    f.write(',')
            f.write('\n')
        f.write('}')
        if (r!=(H-1)):
                f.write(',')
        f.write('\n')
        f.write('//----------------------------------------------')
        f.write('\t\n')
    f.write("};"+ "\n")

    f.write("#endif")
    f.close()

