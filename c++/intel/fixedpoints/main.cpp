// Add M and C in matmul function

#include <iostream>
#include <fstream>
#include <stdio.h>
#include <chrono>
#include <algorithm>
#include "input_kernel_vars.h"
#include "xformed_equations.h"

using namespace std;
int countMuls = 0;
//double tp1 = 0;
double input_transform =0;
double kernel_transform =0;
double pairwise_mult =0;
double delete_time =0;
std::string vect_mul_name ="";
double final_transform =0;

int main() {
	
	 auto startall = chrono::high_resolution_clock::now();
	
	// Benchmark to analyse Input Xformation effeciency 
	// Increase test time to compute accurate Benchmark
	const int timeTest = 100;
	const int vecMul = 1;			// 0 for Direct Mat Mul and 1 for Vector based Mat Mul
	float timeTaken[timeTest] = {}; 

	// Pad the input file with zeros in a temporary array

	int8_t X_padded[input_len + 2][input_len + 2][C] = {};
	
	for (int h = 0; h < (input_len); h++) {
		for (int w = 0; w < (input_len); w++) {
			for (int ch = 0; ch < C; ch++) {
				X_padded[h + 1][w + 1][ch] = X_in[h][w][ch];
			}}}

	// // Extract the inputs
	// // Input arrays
	int8_t X[in_rows][in_cols][(input_len / o_rows)*(input_len / o_cols)][C] = {};
	
	for (int r = 0; r < (in_rows); r++) {
		for (int c = 0; c < (in_cols); c++) {
			int hw = 0;
			for (int image_rows = 0; image_rows < (input_len); image_rows = image_rows + 2) {
				for (int image_cols = 0; image_cols < (input_len); image_cols = image_cols + 2) {
					for (int ch = 0; ch < C; ch++) {
						X[r][c][hw][ch] = X_padded[r + image_rows][c + image_cols][ch];
					}
					hw++;}}}}
	
	// Transformed Input array  ( BT *x *B ) for all the channels
	// Create a dynamic array
	auto X_xformed = new fixed_point_t[in_rows][in_cols][(input_len / o_rows)*(input_len / o_cols)][C]();

	// Start the benchmark
	for (int t = 0; t < timeTest; t++) {
		auto start = chrono::high_resolution_clock::now();
		for (int r = 0; r < (in_rows); r++) {
			for (int c = 0; c < (in_cols); c++) {
				for (int hw = 0; hw < ((input_len / 2)*(input_len / 2)); hw++) {
					for (int ch = 0; ch < C; ch++) {
						if		((r == 0)&(c == 0)) { X_xformed[r][c][hw][ch] = X_XFORMED_00; }
						else if ((r == 0)&(c == 1)) { X_xformed[r][c][hw][ch] = X_XFORMED_01; }
						else if ((r == 0)&(c == 2)) { X_xformed[r][c][hw][ch] = X_XFORMED_02; }
						else if ((r == 0)&(c == 3)) { X_xformed[r][c][hw][ch] = X_XFORMED_03; }

						else if ((r == 1)&(c == 0)) { X_xformed[r][c][hw][ch] = X_XFORMED_10; }
						else if ((r == 1)&(c == 1)) { X_xformed[r][c][hw][ch] = X_XFORMED_11; }
						else if ((r == 1)&(c == 2)) { X_xformed[r][c][hw][ch] = X_XFORMED_12; }
						else if ((r == 1)&(c == 3)) { X_xformed[r][c][hw][ch] = X_XFORMED_13; }

						else if ((r == 2)&(c == 0)) { X_xformed[r][c][hw][ch] = X_XFORMED_20; }
						else if ((r == 2)&(c == 1)) { X_xformed[r][c][hw][ch] = X_XFORMED_21; }
						else if ((r == 2)&(c == 2)) { X_xformed[r][c][hw][ch] = X_XFORMED_22; }
						else if ((r == 2)&(c == 3)) { X_xformed[r][c][hw][ch] = X_XFORMED_23; }

						else if ((r == 3)&(c == 0)) { X_xformed[r][c][hw][ch] = X_XFORMED_30; }
						else if ((r == 3)&(c == 1)) { X_xformed[r][c][hw][ch] = X_XFORMED_31; }
						else if ((r == 3)&(c == 2)) { X_xformed[r][c][hw][ch] = X_XFORMED_32; }
						else if ((r == 3)&(c == 3)) { X_xformed[r][c][hw][ch] = X_XFORMED_33; }
						else break;
						
					}
				}
			}
		}
		
		// total computation here
		auto end = chrono::high_resolution_clock::now();

		chrono::duration<double> elapsed_seconds = end - start;
		timeTaken[t] = elapsed_seconds.count();
	}

	sort(timeTaken, timeTaken + timeTest);
	cout << " Stage1: Input Xform takes " <<(timeTaken[(timeTest/2)-1]) << " secs " << endl;
	input_transform = (timeTaken[(timeTest/2)-1]);
	
	// Transformed Kernel array  (G *h *GT) for all the channels and all the M
	auto K_xformed = new fixed_point_t[in_rows][in_cols][M][C]();
	
	
	// Start the benchmark
	for (int t = 0; t < timeTest; t++) {
		auto start = chrono::high_resolution_clock::now();
		for (int r = 0; r < (in_rows); r++) {
			for (int c = 0; c < (in_cols); c++) {
				for (int m = 0; m < M; m++) {
					for (int ch = 0; ch < C; ch++) {
						if		((r == 0)&(c == 0)) { K_xformed[r][c][m][ch] = K_XFORMED_00; }
						else if ((r == 0)&(c == 1)) { K_xformed[r][c][m][ch] = K_XFORMED_01; }
						else if ((r == 0)&(c == 2)) { K_xformed[r][c][m][ch] = K_XFORMED_02; }
						else if ((r == 0)&(c == 3)) { K_xformed[r][c][m][ch] = K_XFORMED_03; }

						else if ((r == 1)&(c == 0)) { K_xformed[r][c][m][ch] = K_XFORMED_10; }
						else if ((r == 1)&(c == 1)) { K_xformed[r][c][m][ch] = K_XFORMED_11; }
						else if ((r == 1)&(c == 2)) { K_xformed[r][c][m][ch] = K_XFORMED_12; }
						else if ((r == 1)&(c == 3)) { K_xformed[r][c][m][ch] = K_XFORMED_13; }

						else if ((r == 2)&(c == 0)) { K_xformed[r][c][m][ch] = K_XFORMED_20; }
						else if ((r == 2)&(c == 1)) { K_xformed[r][c][m][ch] = K_XFORMED_21; }
						else if ((r == 2)&(c == 2)) { K_xformed[r][c][m][ch] = K_XFORMED_22; }
						else if ((r == 2)&(c == 3)) { K_xformed[r][c][m][ch] = K_XFORMED_23; }

						else if ((r == 3)&(c == 0)) { K_xformed[r][c][m][ch] = K_XFORMED_30; }
						else if ((r == 3)&(c == 1)) { K_xformed[r][c][m][ch] = K_XFORMED_31; }
						else if ((r == 3)&(c == 2)) { K_xformed[r][c][m][ch] = K_XFORMED_32; }
						else if ((r == 3)&(c == 3)) { K_xformed[r][c][m][ch] = K_XFORMED_33; }
						else break;
					}
				}
			}
		}
		//cout << "Here" <<endl;
		// total computation here
		auto end = chrono::high_resolution_clock::now();

		chrono::duration<double> elapsed_seconds = end - start;
		timeTaken[t] = elapsed_seconds.count();
	}

	sort(timeTaken, timeTaken + timeTest);
	cout << " Stage2: Ker Xform takes " <<(timeTaken[(timeTest/2)-1]) << " secs " << endl;
	kernel_transform = (timeTaken[(timeTest/2)-1]);


	auto XK = new fixed_point_t[in_rows][in_cols][M][input_len / o_rows][input_len / o_cols]();
	
	// Start the benchmark
	for (int t = 0; t < timeTest; t++) {
		auto start = chrono::high_resolution_clock::now();
		countMuls = 0;
		delete_time =0;
		// Matrix Multiplication
		for (int r = 0; r < 4; r++) {
			for (int c = 0; c < 4; c++) {
					
					if(vecMul==1){
						// 1Vector Based Matrix Multiplication
						vect_matmul_func3(X_xformed[r][c], K_xformed[r][c], XK[r][c]);
					}
					else{
						// Matrix Multiplication
						matmul_func(X_xformed[r][c], K_xformed[r][c], XK[r][c]);
					}
					
					}}
		// total computation here
		auto end = chrono::high_resolution_clock::now();

		chrono::duration<double> elapsed_seconds = end - start;
		timeTaken[t] = elapsed_seconds.count()-delete_time;
		//cout << " Deleted time is " << delete_time <<endl;
	}
	
	cout << " Total Number of Multiplies are: " << countMuls << endl;
	//cout << (tp1/timeTest) << endl;
	
	const float intelProc = 2600000000;
	const float armProc = 1200000000;
	
	sort(timeTaken, timeTaken + timeTest);
	if(vecMul==1){
		cout << " Stage3: Vector Based MatMul takes " <<(timeTaken[(timeTest/2)-1]) << " secs " << endl;
		cout << " Cycles/ Multiply: "<< (((intelProc)*(timeTaken[(timeTest/2)-1]))/countMuls)/4 << endl;
		}
	else {
		cout << " Stage3: Direct MatMul takes " <<(timeTaken[(timeTest/2)-1]) << " secs " << endl;
		cout << " Cycles/ Multiply: "<< ((intelProc)*(timeTaken[(timeTest/2)-1]))/countMuls << endl;
		}
	pairwise_mult = (timeTaken[(timeTest/2)-1]);
	
	// Delete the temp array
	delete[] X_xformed;
	delete[] K_xformed;

	// Perform final transformation to get the convulation result
	auto temp = new fixed_point_t[o_rows][o_cols][M][input_len/o_rows][input_len/o_cols]();
	
	// Start the benchmark
	for (int t = 0; t < timeTest; t++) {
		auto start = chrono::high_resolution_clock::now();
		for (int r = 0; r < (o_rows); r++) {
			for (int c = 0; c < (o_cols); c++) {
				for (int m = 0; m < M; m++) {
						for (int numb_r = 0; numb_r < (input_len / 2); numb_r++) {
							for (int numb_c = 0; numb_c < (input_len / 2); numb_c++) {
								if ((r == 0)&(c == 0)) { temp[r][c][m][numb_r][numb_c] = XK_XFORMED_00; }
								else if ((r == 0)&(c == 1)) { temp[r][c][m][numb_r][numb_c] = XK_XFORMED_01; }
								else if ((r == 1)&(c == 0)) { temp[r][c][m][numb_r][numb_c] = XK_XFORMED_10; }
								else if ((r == 1)&(c == 1)) { temp[r][c][m][numb_r][numb_c] = XK_XFORMED_11; }
								else break;
								//cout << temp[r][c][m][numb_r][numb_c] << '\t';
							}
							//cout << endl;
						}
						//cout << "---------------" << endl;
					}
			}
			//cout << "*****************************************************************************" << endl;
		}
		
		// total computation here
		auto end = chrono::high_resolution_clock::now();

		chrono::duration<double> elapsed_seconds = end - start;
		//cout << "elapsed time: " << elapsed_seconds.count() << "s\n";
		timeTaken[t] = elapsed_seconds.count();
		//cout << timeTaken[t] << endl;
	}

	sort(timeTaken, timeTaken + timeTest);
	cout << " Stage4: Final Xform takes " <<(timeTaken[(timeTest/2)-1]) << " secs " << endl;
	final_transform = (timeTaken[(timeTest/2)-1]);
	delete[] XK;
	
	// Place the result in a single array
	//fixed_point_t result[M][input_len][input_len] = {};
	auto result = new fixed_point_t[M][input_len][input_len]();
	for (int m = 0; m < M; m++) {
		int rr = 0;
		for (int x_no_r = 0; x_no_r < (input_len / 2); x_no_r++) {
			for (int r = 0; r < 2; r++) {
				int cc = 0;
				for (int x_no_c = 0; x_no_c < (input_len / 2); x_no_c++) {
					for (int c = 0; c < 2; c++) {
						result[m][r + rr][c + cc] = (temp[r][c][m][x_no_r][x_no_c]) /4;
						//cout << result[m][r + rr][c + cc] << '\t';
					}
					cc = cc + 2;
					//cout << endl;
				}
			}
			rr = rr + 2;
			//cout << "---------------" << endl;
		}
		//cout << "*****************************************************************************" << endl;
	}
	
	// total computation here
	auto endall = chrono::high_resolution_clock::now();
	chrono::duration<double> elapsed_seconds = endall - startall;
	cout << "Full Program Time Takes : " << elapsed_seconds.count() << "s\n";
	
	// Delete the temp array
	delete[] temp;

	// Write to a file
	//ofstream myfile("results1.txt");
	ofstream myfile(fr);
	if (myfile.is_open())
	{
		// print the final conv result
		for (int m = 0; m < M; m++) {
			for (int i = 0; i < (input_len); i++) {
				for (int j = 0; j < (input_len); j++) {
					myfile <<  (result[m][i][j]) << '\t';
				}
				myfile << '\n';
			}
			myfile << "---------------------------------" << '\n';
		}
		myfile.close();
	}
	else cout << "Unable to open file";
	
	// Write to a file
	ofstream mylog("result_log.txt",ios_base::app);
	if (mylog.is_open())
	{
		mylog << arch <<  " Arch LayerName: " << layerName << endl;
		mylog << " *************************************"<< endl;
		
		mylog << " Input Transform takes : " << input_transform << " seconds" << endl;
		mylog << " Kernel Transform takes : " << kernel_transform << " seconds" << endl;
		mylog << " Total Multiplies are : " << countMuls << endl;
		if(vecMul==1){
			mylog << ::vect_mul_name << endl;
			cout << " Vector Based Pairwise_Mul takes " <<pairwise_mult << " secs " << endl;
			cout << " Cycles/ Multiply: "<< (((intelProc)*pairwise_mult)/countMuls)/4 << endl;
			mylog << " Vector Based Pairwise Mul takes : " << pairwise_mult << " seconds" << endl;
			mylog << " Cycles/ Multiply: "<< (((intelProc)*pairwise_mult)/countMuls)/4 << endl;
			mylog << " Multiply/Cycle: "<< 1/((((intelProc)*pairwise_mult)/countMuls)/4) << endl;
		}
		else {
			cout << " Direct MatMul takes " <<pairwise_mult << " secs " << endl;
			cout << " Cycles/ Multiply: "<< ((intelProc)*pairwise_mult)/countMuls << endl;
			mylog << " Matrix Multiplication takes : " << pairwise_mult << " seconds" << endl;
			mylog << " Cycles/ Multiply: "<< ((intelProc)*pairwise_mult)/countMuls << endl;
			mylog << " Multiply/Cycle: "<< 1/(((intelProc)*pairwise_mult)/countMuls) << endl;
		}
		mylog << " Final Transform takes : " << final_transform << " seconds" << endl;
		mylog << " Total Algorithm takes : " << (input_transform+kernel_transform+pairwise_mult+final_transform) << " seconds" << endl;
		mylog <<  " --------------------- " << endl;
		mylog.close();
	}
	else cout << "Unable to open file";
		
	cout<< "hereeee" << endl;
	// Delete the temp array
	delete[] result;
	
	getchar();
	return 0;
}

void matmul_func(fixed_point_t A[(input_len / 2)*(input_len / 2)][C], fixed_point_t B[M][C], fixed_point_t output[M][input_len / o_rows][input_len / o_cols]){ 
	fixed_point_t temp[M][(input_len / 2)*(input_len / 2)] = {};
	for (int m = 0; m < M; m++){	
		for (int hw = 0; hw < ((input_len / 2)*(input_len / 2)); hw++) {
			for (int ch = 0; ch < C; ch++) {
					temp[m][hw] = temp[m][hw] + (A[hw][ch]* B[m][ch]);
					::countMuls++;
			}
		}
	}
	
	for (int m = 0; m < M; m++){
		int y=0;
		for (int numb_r = 0; numb_r < (input_len / 2); numb_r++) {
			for (int numb_c = 0; numb_c < (input_len / 2); numb_c++) {
				output[m][numb_r][numb_c]= temp[m][y];
				y++;
				//cout << output[m][numb_r][numb_c] << '\t';		
			}
			//cout << endl;
		}
		//cout << endl;
		//cout << "--------------------------------------------" << endl;
	}
	

}
void vect_matmul_func1(fixed_point_t A[(input_len / 2)*(input_len / 2)][C], fixed_point_t B[M][C], fixed_point_t output[M][input_len / o_rows][input_len / o_cols]){ 
	// 2 vector load and 1 Muls
	::vect_mul_name  = " <<<<<<< 2 vector load and 1 Mul >>>>>>>>> ";
	v4si a4,b4;
	v4si c4 = {};
	v4si *p;
	//fixed_point_t temp[M][(input_len / 2)*(input_len / 2)] = {};
	auto temp = new fixed_point_t[M][(input_len / 2)*(input_len / 2)] ();
	//cout << "hello" << endl;
	for (int m = 0; m < M; m++){
		for (int hw = 0; hw < ((input_len / 2)*(input_len / 2)); hw++) {
		v4si sum4 = {};
			for (int ch = 0; ch < C; ch = ch +4) {
				p = (v4si *)& A[hw][ch]; 		// p will have the address of in[hw][ch] 
												// (v4si *) cast from int* to v4si*
				a4 = *p; 						// a4 gets contents of p 
				p = (v4si *)& B[m][ch];
				b4 = *p;
				
				// Multiply 4 Input vals to 4 kernel vals of different using vectors
				c4 =(a4*b4);	
				::countMuls+=4;
				sum4 = sum4 + c4;
				//cout << hw << endl;
			}
			// Add each of these vals
			for (int u = 0; u < 4; u++) {
				temp[m][hw] = temp[m][hw] + sum4[u];
			}
	}
	}
	

	for (int m = 0; m < M; m++){
		int y=0;
		for (int numb_r = 0; numb_r < (input_len / 2); numb_r++) {
			for (int numb_c = 0; numb_c < (input_len / 2); numb_c++) {
				output[m][numb_r][numb_c]= temp[m][y];
				y++;
				//cout << output[m][numb_r][numb_c] << '\t';		
			}
			//cout << endl;
		}
		//cout << endl;
		//cout << "--------------------------------------------" << endl;
	}

}
void vect_matmul_func2(fixed_point_t A[(input_len / 2)*(input_len / 2)][C], fixed_point_t B[M][C], fixed_point_t output[M][input_len / o_rows][input_len / o_cols]){ 



	// 4 vector load and 2 Muls
	::vect_mul_name  = " <<<<<<< 4 vector load and 2 Mul >>>>>>>>> ";
	v4si a4_0,a4_1,*p0,b4_0,b4_1,c4_0,c4_1,c4_2,c4_3;
	fixed_point_t temp[M][(input_len / 2)*(input_len / 2)] = {};
	
	for (int m = 0; m < M; m++){
		for (int hw = 0; hw < ((input_len / 2)*(input_len / 2)); hw++) {
		v4si sum4_0 = {};
		v4si sum4_1 = {};
			for (int ch = 0; ch < C; ch = ch +8) {
			
			p0 = (v4si *)& A[hw][ch]; 		
			a4_0 = *p0; 
						
			p0 = (v4si *)& A[hw][ch+4]; 
			a4_1 = *p0;

			p0 = (v4si *)& B[m][ch];
			b4_0 = *p0;
			
			p0 = (v4si *)& B[m][ch+4];
			b4_1 = *p0;
			
			c4_0 = a4_0 * b4_0;
			c4_1 = a4_1 * b4_1;
			::countMuls += 8;
				

			sum4_0 = sum4_0 + c4_0;
			sum4_1 = sum4_1 + c4_1;
			}
			// Add each of these vals
			for (int u = 0; u < 4; u++) {
				temp[m][hw] = temp[m][hw] + sum4_0[u]+ sum4_1[u];
			}
	}
	}
	

	for (int m = 0; m < M; m++){
		int y=0;
		for (int numb_r = 0; numb_r < (input_len / 2); numb_r++) {
			for (int numb_c = 0; numb_c < (input_len / 2); numb_c++) {
				output[m][numb_r][numb_c]= temp[m][y];
				y++;
				//cout << output[m][numb_r][numb_c] << '\t';		
			}
			//cout << endl;
		}
		//cout << endl;
		//cout << "--------------------------------------------" << endl;
	}

}
void vect_matmul_func3(fixed_point_t A[(input_len / 2)*(input_len / 2)][C], fixed_point_t B[M][C], fixed_point_t output[M][input_len / o_rows][input_len / o_cols]){ 
	// 5  Loads and 4 Muls
	//float time = 0;
	::vect_mul_name  = " <<<<<<< 5 vector load and 4 Mul >>>>>>>>> ";
	fixed_point_t temp[M][hw_pad] = {};
	fixed_point_t At[C][hw_pad]={};	
	
	auto start2 = chrono::high_resolution_clock::now();
	for (int hw = 0; hw < ((input_len / 2)*(input_len / 2)); hw++) {
		for (int ch = 0; ch < C; ch++){
				At[ch][hw] = A[hw][ch];}}
	fixed_point_t Bt[C][M]={};	
	for (int m = 0; m < M; m++) {
		for (int ch = 0; ch < C; ch++){
				Bt[ch][m] = B[m][ch];}}
	// for (int ch = 0; ch < C; ch++){
		// for (int hw = 0; hw < ((input_len / 2)*(input_len / 2)); hw++) {
			// cout << At[ch][hw]<< " ";
			// //cout << B[ch] << "    ";
		// }
		// cout << endl;
		// cin.get();
	// }
	auto end2 = chrono::high_resolution_clock::now();
	chrono::duration<double> elapsed_seconds = end2 - start2;
	::delete_time += elapsed_seconds.count();
	
	v4si a4,b4,b4_0,b4_1,b4_2,b4_3,*p;
	v4si c4 = {};
	
	for (int m = 0; m < M; m = m+4){
		for (int hw = 0; hw < hw_pad; hw= hw+4) {
			v4si sum4_0 = {};
			v4si sum4_1 = {};
			v4si sum4_2 = {};
			v4si sum4_3 = {};
			for (int ch = 0; ch < C; ch++) {
				p = (v4si *)& At[ch][hw]; 		// p will have the address of in[hw][ch] 
												// (v4si *) cast from int* to v4si*
				a4 = *p; 						// a4 gets contents of p 
				
				p = (v4si *)& Bt[ch][m];			// B={1,2,3,4}
				b4 = *p;							
				
				for (int u = 0; u < 4; u++) {
					b4_0[u] = b4[0];			//{1,1,1,1}
					b4_1[u] = b4[1];			//{2,2,2,2}
					b4_2[u] = b4[2];			//{3,3,3,3}
					b4_3[u] = b4[3];			//{4,4,4,4}
					//cout << b4_0[u] << " ";
				}
				//cout<<endl;
				//cin.get();
				sum4_0 = sum4_0 + (a4 * b4_0);
				sum4_1 = sum4_1 + (a4 * b4_1);
				sum4_2 = sum4_2	+ (a4 * b4_2);
				sum4_3 = sum4_3 + (a4 * b4_3);
				::countMuls+=16;
			}
			for (int u = 0; u < 4; u++) {
				temp[m][hw+u] 	= sum4_0[u];
				temp[m+1][hw+u] = sum4_1[u];
				temp[m+2][hw+u] = sum4_2[u];
				temp[m+3][hw+u] = sum4_3[u];
			}
		}
	}	
	
	//auto end2 = chrono::high_resolution_clock::now();
	//chrono::duration<double> elapsed_seconds = end2 - start2;
	//cout << elapsed_seconds.count()<< endl;

	for (int m = 0; m < M; m++){
		int y=0;
		for (int numb_r = 0; numb_r < (input_len / 2); numb_r++) {
			for (int numb_c = 0; numb_c < (input_len / 2); numb_c++) {
				output[m][numb_r][numb_c]= temp[m][y];
				y++;
				//cout << y << '\t';
			}
			//cout << endl;
		}
	//cout << endl;
	}
	//cin.get();

}

