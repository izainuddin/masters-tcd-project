#ifndef INPUTSKERNELVARS_H
#define INPUTSKERNELVARS_H

//#include "../../input-kernel-data/input_data_c++/inputs_GoogLeNet_convolution2.h"
//#include "../../input-kernel-data/kernel_data_c++/kernels_GoogLeNet_convolution2.h"

//#include "../../input-kernel-data/input_data_c++/inputs_GoogLeNet_inception(3a).h"
//#include "../../input-kernel-data/kernel_data_c++/kernels_GoogLeNet_inception(3a).h"

//#include "../../input-kernel-data/input_data_c++/inputs_GoogLeNet_inception(3b).h"
//#include "../../input-kernel-data/kernel_data_c++/kernels_GoogLeNet_inception(3b).h"

//#include "../../input-kernel-data/input_data_c++/inputs_GoogLeNet_inception(4a).h"
//#include "../../input-kernel-data/kernel_data_c++/kernels_GoogLeNet_inception(4a).h"

//#include "../../input-kernel-data/input_data_c++/inputs_GoogLeNet_inception(4b).h"
//#include "../../input-kernel-data/kernel_data_c++/kernels_GoogLeNet_inception(4b).h"

//#include "../../input-kernel-data/input_data_c++/inputs_GoogLeNet_inception(4c).h"
//#include "../../input-kernel-data/kernel_data_c++/kernels_GoogLeNet_inception(4c).h"

//#include "../../input-kernel-data/input_data_c++/inputs_GoogLeNet_inception(4d).h"
//#include "../../input-kernel-data/kernel_data_c++/kernels_GoogLeNet_inception(4d).h"

//#include "../../input-kernel-data/input_data_c++/inputs_GoogLeNet_inception(4e).h"
//#include "../../input-kernel-data/kernel_data_c++/kernels_GoogLeNet_inception(4e).h"

//#include "../../input-kernel-data/input_data_c++/inputs_GoogLeNet_inception(5a).h"
//#include "../../input-kernel-data/kernel_data_c++/kernels_GoogLeNet_inception(5a).h"

//#include "../../input-kernel-data/input_data_c++/inputs_GoogLeNet_inception(5b).h"
//#include "../../input-kernel-data/kernel_data_c++/kernels_GoogLeNet_inception(5b).h"

#include "../../input-kernel-data/input_data_c++/inputs_AlexNet_convolution3.h"
#include "../../input-kernel-data/kernel_data_c++/kernels_AlexNet_convolution3.h"

//#include "../../input-kernel-data/input_data_c++/inputs_AlexNet_convolution4.h"
//#include "../../input-kernel-data/kernel_data_c++/kernels_AlexNet_convolution4.h"

//#include "../../input-kernel-data/input_data_c++/inputs_AlexNet_convolution5.h"
//#include "../../input-kernel-data/kernel_data_c++/kernels_AlexNet_convolution5.h"


//#include "../../input-kernel-data/input_data_c++/inputs_ResNet50_convolution2_x.h"
//#include "../../input-kernel-data/kernel_data_c++/kernels_ResNet50_convolution2_x.h"


//#include "../../input-kernel-data/input_data_c++/inputs_ResNet50_convolution3_x.h"
//#include "../../input-kernel-data/kernel_data_c++/kernels_ResNet50_convolution3_x.h"


//#include "../../input-kernel-data/input_data_c++/inputs_ResNet50_convolution4_x.h"
//#include "../../input-kernel-data/kernel_data_c++/kernels_ResNet50_convolution4_x.h"

// This header file contains all the values such as C, M, kernel values.

const int in_rows = 4;
const int in_cols = 4;
const int k_rows = 3;
const int k_cols = 3;

// resulting matrix 's' row and column
const int o_rows = in_rows - k_rows + 1;
const int o_cols = in_cols - k_cols + 1;

// Fixed-point Format
typedef int fixed_point_t;

// For 8 bit Fixed Point, 1 bit for signed bit 
#define FIXED_POINT_FRACTIONAL_BITS 7

typedef int v4si __attribute__ ((vector_size (16)));


void matmul_func(fixed_point_t A[(input_len / 2)*(input_len / 2)][C], fixed_point_t B[M][C], fixed_point_t output[M][input_len / o_rows][input_len / o_cols]) ;
void vect_matmul_func1(fixed_point_t A[(input_len / 2)*(input_len / 2)][C], fixed_point_t B[M][C], fixed_point_t output[M][input_len / o_rows][input_len / o_cols]) ;
void vect_matmul_func2(fixed_point_t A[(input_len / 2)*(input_len / 2)][C], fixed_point_t B[M][C], fixed_point_t output[M][input_len / o_rows][input_len / o_cols]) ;
void vect_matmul_func3(fixed_point_t A[(input_len / 2)*(input_len / 2)][C], fixed_point_t B[M][C], fixed_point_t output[M][input_len / o_rows][input_len / o_cols]) ;
#endif