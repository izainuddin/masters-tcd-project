#ifndef INPUTSKERNELDATA_H
#define INPUTSKERNELDATA_H

#include "inputs.h"
// This header file contains all the values such as C, M, kernel values.

const int in_rows = 4;
const int in_cols = 4;
const int k_rows = 3;
const int k_cols = 3;

// resulting matrix 's' row and column
const int o_rows = in_rows - k_rows + 1;
const int o_cols = in_cols - k_cols + 1;

// Fixed-point Format
typedef int fixed_point_t;

// For 8 bit Fixed Point, 1 bit for signed bit 
#define FIXED_POINT_FRACTIONAL_BITS 7

typedef int v4si __attribute__ ((vector_size (16)));


void matmul_func(fixed_point_t A[(input_len / 2)*(input_len / 2)][C], fixed_point_t B[C], fixed_point_t output[input_len / o_rows][input_len / o_cols]) ;
void vect_matmul_func(fixed_point_t A[(input_len / 2)*(input_len / 2)][C], fixed_point_t B[C], fixed_point_t output[input_len / o_rows][input_len / o_cols]) ;
void neon_matmul_func(fixed_point_t A[(input_len / 2)*(input_len / 2)][C], fixed_point_t B[C], fixed_point_t output[input_len / o_rows][input_len / o_cols]);


#endif
