// 5 Loads and 16 Multiplies in One Go

#include <iostream>
#include <fstream>
#include <stdio.h>
#include <chrono>
#include <algorithm>
#include <arm_neon.h> 
#include "input_kernel_data.h"
#include "inputs.h"
#include "kernels.h"
#include "xformed_equations.h"

using namespace std;
int countMuls = 0;
//double tp1 = 0;

int main() {
	
	 auto startall = chrono::high_resolution_clock::now();
	
	// Benchmark to analyse Input Xformation effeciency 
	// Increase test time to compute accurate Benchmark
	const int timeTest = 1;
	const int matMulMethod = 2;			// 0 for Direct Mat Mul, 1 for Vector based Mat Mul and 2 for Neon Based Mat Mul
	float timeTaken[timeTest] = {}; 

	// Pad the input file with zeros in a temporary array

	int8_t X_padded[input_len + 2][input_len + 2][C] = {};
	
	for (int h = 0; h < (input_len); h++) {
		for (int w = 0; w < (input_len); w++) {
			for (int ch = 0; ch < C; ch++) {
				X_padded[h + 1][w + 1][ch] = X_in[h][w][ch];
			}}}

	// // Extract the inputs
	// // Input arrays
	int8_t X[in_rows][in_cols][(input_len / o_rows)*(input_len / o_cols)][C] = {};
	
	for (int r = 0; r < (in_rows); r++) {
		for (int c = 0; c < (in_cols); c++) {
			int hw = 0;
			for (int image_rows = 0; image_rows < (input_len); image_rows = image_rows + 2) {
				for (int image_cols = 0; image_cols < (input_len); image_cols = image_cols + 2) {
					for (int ch = 0; ch < C; ch++) {
						X[r][c][hw][ch] = X_padded[r + image_rows][c + image_cols][ch];
					}
					hw++;}}}}
	
	// Transformed Input array  ( BT *x *B ) for all the channels
	// Create a dynamic array
	auto X_xformed = new fixed_point_t[in_rows][in_cols][(input_len / o_rows)*(input_len / o_cols)][C]();

	// Start the benchmark
	for (int t = 0; t < timeTest; t++) {
		auto start = chrono::high_resolution_clock::now();
		for (int r = 0; r < (in_rows); r++) {
			for (int c = 0; c < (in_cols); c++) {
				for (int hw = 0; hw < ((input_len / 2)*(input_len / 2)); hw++) {
					for (int ch = 0; ch < C; ch++) {
						if		((r == 0)&(c == 0)) { X_xformed[r][c][hw][ch] = X_XFORMED_00; }
						else if ((r == 0)&(c == 1)) { X_xformed[r][c][hw][ch] = X_XFORMED_01; }
						else if ((r == 0)&(c == 2)) { X_xformed[r][c][hw][ch] = X_XFORMED_02; }
						else if ((r == 0)&(c == 3)) { X_xformed[r][c][hw][ch] = X_XFORMED_03; }

						else if ((r == 1)&(c == 0)) { X_xformed[r][c][hw][ch] = X_XFORMED_10; }
						else if ((r == 1)&(c == 1)) { X_xformed[r][c][hw][ch] = X_XFORMED_11; }
						else if ((r == 1)&(c == 2)) { X_xformed[r][c][hw][ch] = X_XFORMED_12; }
						else if ((r == 1)&(c == 3)) { X_xformed[r][c][hw][ch] = X_XFORMED_13; }

						else if ((r == 2)&(c == 0)) { X_xformed[r][c][hw][ch] = X_XFORMED_20; }
						else if ((r == 2)&(c == 1)) { X_xformed[r][c][hw][ch] = X_XFORMED_21; }
						else if ((r == 2)&(c == 2)) { X_xformed[r][c][hw][ch] = X_XFORMED_22; }
						else if ((r == 2)&(c == 3)) { X_xformed[r][c][hw][ch] = X_XFORMED_23; }

						else if ((r == 3)&(c == 0)) { X_xformed[r][c][hw][ch] = X_XFORMED_30; }
						else if ((r == 3)&(c == 1)) { X_xformed[r][c][hw][ch] = X_XFORMED_31; }
						else if ((r == 3)&(c == 2)) { X_xformed[r][c][hw][ch] = X_XFORMED_32; }
						else if ((r == 3)&(c == 3)) { X_xformed[r][c][hw][ch] = X_XFORMED_33; }
						else break;
						
					}
				}
			}
		}
		
		// total computation here
		auto end = chrono::high_resolution_clock::now();

		chrono::duration<double> elapsed_seconds = end - start;
		timeTaken[t] = elapsed_seconds.count();
	}

	sort(timeTaken, timeTaken + timeTest);
	cout << " Stage1: Input Xform takes " <<(timeTaken[(timeTest/2)-1]) << " secs " << endl;

	// Transformed Kernel array  (G *h *GT) for all the channels and all the M
	auto K_xformed = new fixed_point_t[in_rows][in_cols][M][C]();
	
	// Start the benchmark
	for (int t = 0; t < timeTest; t++) {
		auto start = chrono::high_resolution_clock::now();
		for (int r = 0; r < (in_rows); r++) {
			for (int c = 0; c < (in_cols); c++) {
				for (int m = 0; m < M; m++) {
					for (int ch = 0; ch < C; ch++) {
						if		((r == 0)&(c == 0)) { K_xformed[r][c][m][ch] = K_XFORMED_00; }
						else if ((r == 0)&(c == 1)) { K_xformed[r][c][m][ch] = K_XFORMED_01; }
						else if ((r == 0)&(c == 2)) { K_xformed[r][c][m][ch] = K_XFORMED_02; }
						else if ((r == 0)&(c == 3)) { K_xformed[r][c][m][ch] = K_XFORMED_03; }

						else if ((r == 1)&(c == 0)) { K_xformed[r][c][m][ch] = K_XFORMED_10; }
						else if ((r == 1)&(c == 1)) { K_xformed[r][c][m][ch] = K_XFORMED_11; }
						else if ((r == 1)&(c == 2)) { K_xformed[r][c][m][ch] = K_XFORMED_12; }
						else if ((r == 1)&(c == 3)) { K_xformed[r][c][m][ch] = K_XFORMED_13; }

						else if ((r == 2)&(c == 0)) { K_xformed[r][c][m][ch] = K_XFORMED_20; }
						else if ((r == 2)&(c == 1)) { K_xformed[r][c][m][ch] = K_XFORMED_21; }
						else if ((r == 2)&(c == 2)) { K_xformed[r][c][m][ch] = K_XFORMED_22; }
						else if ((r == 2)&(c == 3)) { K_xformed[r][c][m][ch] = K_XFORMED_23; }

						else if ((r == 3)&(c == 0)) { K_xformed[r][c][m][ch] = K_XFORMED_30; }
						else if ((r == 3)&(c == 1)) { K_xformed[r][c][m][ch] = K_XFORMED_31; }
						else if ((r == 3)&(c == 2)) { K_xformed[r][c][m][ch] = K_XFORMED_32; }
						else if ((r == 3)&(c == 3)) { K_xformed[r][c][m][ch] = K_XFORMED_33; }
						else break;
					}
				}
			}
		}
		// total computation here
		auto end = chrono::high_resolution_clock::now();

		chrono::duration<double> elapsed_seconds = end - start;
		timeTaken[t] = elapsed_seconds.count();
	}

	sort(timeTaken, timeTaken + timeTest);
	cout << " Stage2: Ker Xform takes " <<(timeTaken[(timeTest/2)-1]) << " secs " << endl;

	auto XK = new fixed_point_t[in_rows][in_cols][M][input_len / o_rows][input_len / o_cols]();
	
	// Start the benchmark
	for (int t = 0; t < timeTest; t++) {
		auto start = chrono::high_resolution_clock::now();
		// Matrix Multiplication
		for (int r = 0; r < 4; r++) {
			for (int c = 0; c < 4; c++) {
				for (int m = 0; m < M; m++) {
					
					if(matMulMethod==1){
						// Vector Based Matrix Multiplication
						vect_matmul_func(X_xformed[r][c], K_xformed[r][c][m], XK[r][c][m]);
					}
					else if (matMulMethod==2){
						// Neon Based Matrix Multiplication
						neon_matmul_func(X_xformed[r][c], K_xformed[r][c][m], XK[r][c][m]);
					}
					else{
						// Matrix Multiplication
						matmul_func(X_xformed[r][c], K_xformed[r][c][m], XK[r][c][m]);
					}
					
					}}}
		// total computation here
		auto end = chrono::high_resolution_clock::now();

		chrono::duration<double> elapsed_seconds = end - start;
		timeTaken[t] = elapsed_seconds.count();
	}
	
	cout << " Total Number of Multiplies are: " << countMuls << endl;
	//cout << (tp1/timeTest) << endl;
	
	const float intelProc = 2600000000;
	const float armProc = 1200000000;
	
	sort(timeTaken, timeTaken + timeTest);
	if(matMulMethod==1){
		cout << " Stage3: Vector Based MatMul takes " <<(timeTaken[(timeTest/2)-1]) << " secs " << endl;
		cout << " Cycles/ Multiply: "<< (((armProc)*(timeTaken[(timeTest/2)-1]))/countMuls)/4 << endl;
		}
	else if(matMulMethod==2){
		cout << " Stage3: Neon Based MatMul takes " <<(timeTaken[(timeTest/2)-1]) << " secs " << endl;
		cout << " Cycles/ Multiply: "<< (((armProc)*(timeTaken[(timeTest/2)-1]))/countMuls)/4 << endl;
		}
	else {
		cout << " Stage3: Direct MatMul takes " <<(timeTaken[(timeTest/2)-1]) << " secs " << endl;
		cout << " Cycles/ Multiply: "<< ((armProc)*(timeTaken[(timeTest/2)-1]))/countMuls << endl;
		}
	
	// Delete the temp array
	delete[] X_xformed;
	delete[] K_xformed;
	
	// Perform final transformation to get the convulation result
	auto temp = new fixed_point_t[o_rows][o_cols][M][input_len/o_rows][input_len/o_cols]();
	
	// Start the benchmark
	for (int t = 0; t < timeTest; t++) {
		auto start = chrono::high_resolution_clock::now();
		for (int r = 0; r < (o_rows); r++) {
			for (int c = 0; c < (o_cols); c++) {
				for (int m = 0; m < M; m++) {
						for (int numb_r = 0; numb_r < (input_len / 2); numb_r++) {
							for (int numb_c = 0; numb_c < (input_len / 2); numb_c++) {
								if ((r == 0)&(c == 0)) { temp[r][c][m][numb_r][numb_c] = XK_XFORMED_00; }
								else if ((r == 0)&(c == 1)) { temp[r][c][m][numb_r][numb_c] = XK_XFORMED_01; }
								else if ((r == 1)&(c == 0)) { temp[r][c][m][numb_r][numb_c] = XK_XFORMED_10; }
								else if ((r == 1)&(c == 1)) { temp[r][c][m][numb_r][numb_c] = XK_XFORMED_11; }
								else break;
								//cout << temp[r][c][m][numb_r][numb_c] << '\t';
							}
							//cout << endl;
						}
						//cout << "---------------" << endl;
					}
			}
			//cout << "*****************************************************************************" << endl;
		}
		
		// total computation here
		auto end = chrono::high_resolution_clock::now();

		chrono::duration<double> elapsed_seconds = end - start;
		//cout << "elapsed time: " << elapsed_seconds.count() << "s\n";
		timeTaken[t] = elapsed_seconds.count();
		//cout << timeTaken[t] << endl;
	}

	sort(timeTaken, timeTaken + timeTest);
	cout << " Stage4: Final Xform takes " <<(timeTaken[(timeTest/2)-1]) << " secs " << endl;
	
	delete[] XK;
	
	// Place the result in a single array
	//fixed_point_t result[M][input_len][input_len] = {};
	auto result = new fixed_point_t[M][input_len][input_len]();
	for (int m = 0; m < M; m++) {
		int rr = 0;
		for (int x_no_r = 0; x_no_r < (input_len / 2); x_no_r++) {
			for (int r = 0; r < 2; r++) {
				int cc = 0;
				for (int x_no_c = 0; x_no_c < (input_len / 2); x_no_c++) {
					for (int c = 0; c < 2; c++) {
						result[m][r + rr][c + cc] = (temp[r][c][m][x_no_r][x_no_c]) /4;
						//cout << result[m][r + rr][c + cc] << '\t';
					}
					cc = cc + 2;
					//cout << endl;
				}
			}
			rr = rr + 2;
			//cout << "---------------" << endl;
		}
		//cout << "*****************************************************************************" << endl;
	}
	
	// Delete the temp array
	delete[] temp;

	// Write to a file
	ofstream myfile("results.txt");
	if (myfile.is_open())
	{
		// print the final conv result
		for (int m = 0; m < M; m++) {
			for (int i = 0; i < (input_len); i++) {
				for (int j = 0; j < (input_len); j++) {
					myfile <<  (result[m][i][j]) << '\t';
				}
				myfile << '\n';
			}
			myfile << "---------------------------------" << '\n';
		}
		myfile.close();
	}
	else cout << "Unable to open file";

	// Delete the temp array
	delete[] result;
	
	// total computation here
	auto endall = chrono::high_resolution_clock::now();
	chrono::duration<double> elapsed_seconds = endall - startall;
	cout << "Full Program Time Takes : " << elapsed_seconds.count() << "s\n";
	
	getchar();
	return 0;
}

void matmul_func(fixed_point_t A[(input_len / 2)*(input_len / 2)][C], fixed_point_t B[C], fixed_point_t output[input_len / o_rows][input_len / o_cols]){ 
	
	fixed_point_t temp[(input_len / 2)*(input_len / 2)] = {};
	for (int hw = 0; hw < ((input_len / 2)*(input_len / 2)); hw++) {
		for (int ch = 0; ch < C; ch++) {
					temp[hw] = temp[hw] + (A[hw][ch]* B[ch]);
					::countMuls++;
		}}	
		
		
	int y=0;
	for (int numb_r = 0; numb_r < (input_len / 2); numb_r++) {
		for (int numb_c = 0; numb_c < (input_len / 2); numb_c++) {
				output[numb_r][numb_c]= temp[y];
				y++;
				//cout << output[numb_r][numb_c] << '\t';
				
		}
		//cout << endl;
	}
	//cout << endl;
}

void vect_matmul_func(fixed_point_t A[(input_len / 2)*(input_len / 2)][C], fixed_point_t B[C], fixed_point_t output[input_len / o_rows][input_len / o_cols]){ 
		
	v4si a4,b4;
	v4si c4 = {};
	v4si *p;
	fixed_point_t temp[(input_len / 2)*(input_len / 2)] = {};
	
	
	for (int hw = 0; hw < ((input_len / 2)*(input_len / 2)); hw++) {
		v4si sum4 = {};
		for (int ch = 0; ch < C; ch = ch +4) {
			p = (v4si *)& A[hw][ch]; 		// p will have the address of in[hw][ch] 
											// (v4si *) cast from int* to v4si*
			a4 = *p; 						// a4 gets contents of p 
			p = (v4si *)& B[ch];
			b4 = *p;
			// Multiply 4 Input vals to 4 kernel vals of different using vectors
			c4 =(a4*b4);	
			::countMuls++;
			sum4 = sum4 + c4;
		}
		// Add each of these vals
		for (int u = 0; u < 4; u++) {
			temp[hw] = temp[hw] + sum4[u];
		}
	}

	int y=0;
	for (int numb_r = 0; numb_r < (input_len / 2); numb_r++) {
		for (int numb_c = 0; numb_c < (input_len / 2); numb_c++) {
				output[numb_r][numb_c]= temp[y];
				y++;
				//cout << output[numb_r][numb_c] << '\t';
		}
		//cout << endl;
	}
	//cout << endl;
}

void neon_matmul_func(fixed_point_t A[(input_len / 2)*(input_len / 2)][C], fixed_point_t B[C], fixed_point_t output[input_len / o_rows][input_len / o_cols]){ 
	
	int hw_padded_size = 52;		// Calculate this in Python and include within input_kernel_Data file (This should be multiples of 4)
	int hw_max = 48;
	int hw_left = 1;
	//fixed_point_t At[hw_padded_size][C]={};
	// for (int hw = 0; hw < ((input_len / 2)*(input_len / 2)); hw++) {
	//	 for (int ch = 0; ch < C; ch++){
	//			 At[hw][ch] = A[hw][ch];}}
				 
	
	fixed_point_t temp[(input_len / 2)*(input_len / 2)] = {};
	
	int32x4_t a4_0,a4_1,a4_2,a4_3,b4,c4_0,c4_1,c4_2,c4_3;
	
	for (int hw = 0; hw < hw_padded_size; hw=hw+4) {
	if (hw < (hw_max))
	{
		int32x4_t sum4_0 = {};
		int32x4_t sum4_1 = {};
		int32x4_t sum4_2 = {};
		int32x4_t sum4_3 = {};
		for (int ch = 0; ch < C; ch = ch +4) {
		
		a4_0 = vld1q_s32 (A[hw]+ch);
		a4_1 = vld1q_s32 (A[hw+1]+ch);
		a4_2 = vld1q_s32 (A[hw+2]+ch);
		a4_3 = vld1q_s32 (A[hw+3]+ch);
		
	
		b4 = vld1q_s32 (B + ch);
		
		c4_0 = a4_0 * b4;
		sum4_0 = sum4_0 + c4_0;
		c4_1 = a4_1 * b4;
		sum4_1 = sum4_1 + c4_1;
		c4_2 = a4_2 * b4;
		sum4_2 = sum4_2 + c4_2;
		c4_3 = a4_3 * b4;
		sum4_3 = sum4_3 + c4_3;
		
		
		/*c4_0 = vmulq_s32(a4_0,b4_0);
		sum4_0 = vaddq_s32(sum4_0,c4_0);
		
		c4_1 = vmulq_s32(a4_1,b4_1);
		sum4_1 = vaddq_s32(sum4_1,c4_1);
		
		
		c4_2 = vmulq_s32(a4_2,b4_2);
		sum4_2 = vaddq_s32(sum4_2,c4_2);
		
		c4_3 = vmulq_s32(a4_3,b4_3);
		sum4_3 = vaddq_s32(sum4_3,c4_3);*/
		
		
		::countMuls = ::countMuls+4;	
		}
		for (int u = 0; u < 4; u++) {
			temp[hw] = temp[hw] + sum4_0[u];
			temp[hw+1] = temp[hw+1] + sum4_1[u];
			temp[hw+2] = temp[hw+2] + sum4_2[u];
			temp[hw+3] = temp[hw+3] + sum4_3[u];
		}
	}
	
	else {
		for (int x = 0; x<hw_left ; x++)
		{
		int32x4_t sum4_0 = {};
		for (int ch = 0; ch < C; ch = ch +4) {
		
		a4_0 = vld1q_s32 (A[hw+x]+ch);
		
	
		b4 = vld1q_s32 (B + ch);
		
		c4_0 = a4_0 * b4;
		sum4_0 = sum4_0 + c4_0;
		
		
		/*c4_0 = vmulq_s32(a4_0,b4_0);
		sum4_0 = vaddq_s32(sum4_0,c4_0);
		*/
		
		
		::countMuls = ::countMuls;	
		}
		for (int u = 0; u < 4; u++) {
			temp[hw+x] = temp[hw+x] + sum4_0[u];
		}
		}

	}
	}

	
	int y=0;
	for (int numb_r = 0; numb_r < (input_len / 2); numb_r++) {
		for (int numb_c = 0; numb_c < (input_len / 2); numb_c++) {
				output[numb_r][numb_c]= temp[y];
				y++;
				//cout << output[numb_r][numb_c] << '\t';
		}
		//cout << endl;
	}
	//cout << endl;
}
